require('./bootstrap')

import Vue from 'vue'
import VueRouter from 'vue-router'

import router from './router/index'
import App from './src/App.vue'

import VueAxios from 'vue-axios';
import axios from 'axios';

Vue.use(VueRouter)
Vue.use(VueAxios, axios);




const app = new Vue({
    el: '#app',
    router: router,

    render: h => h(App),
});
